class People {
    constructor(hoTen, ngaySinh, queQuan) {
        this.hoTen = hoTen;
        this.ngaySinh = ngaySinh;
        this.queQuan = queQuan;
    }
    print() {
        console.log(this.hoTen + " " + this.ngaySinh + " " + this.queQuan);
    }
}

export default People;