import Student from "./student.js";

class CollegeStudent extends Student {
    constructor(hoTen, ngaySinh, queQuan, tenTruong, lop, soDienThoai, chuyenNganh, maSoSinhVien) {
        super(hoTen, ngaySinh, queQuan, tenTruong, lop, soDienThoai);
        this.chuyenNganh = chuyenNganh;
        this.maSoSinhVien = maSoSinhVien;
    }
    print2() {
        console.log(this.chuyenNganh + " " + this.maSoSinhVien);
    }
}

export default CollegeStudent;