import People from "./people.js";

class Worker extends People {
    constructor(hoTen, ngaySinh, queQuan, nganhNghe, noiLamViec, luong) {
        super(hoTen, ngaySinh, queQuan);
        this.nganhNghe = nganhNghe;
        this.noiLamViec = noiLamViec;
        this.luong = luong;
    }
    print3() {
        console.log(this.nganhNghe + " " + this.noiLamViec + " " + this.luong);
    }
}

export default Worker;