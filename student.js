import People from "./people.js";

class Student extends People {
    constructor(hoTen, ngaySinh, queQuan, tenTruong, lop, soDienThoai) {
        super(hoTen, ngaySinh, queQuan);
        this.tenTruong = tenTruong;
        this.lop = lop;
        this.soDienThoai = soDienThoai;
    }
    print1() {
        console.log(this.tenTruong + " " + this.lop + " " + this.soDienThoai);
    }
}

export default Student;