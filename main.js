import People from "./people.js";
import Student from "./student.js";
import CollegeStudent from "./college-student.js";
import Worker from "./worker.js";

let people = new People("Trần Thanh Tú", "25/01/1998", "Đồng Nai");
people.print();
console.log(people instanceof People);

let student = new Student("Trần Thanh Tú", "25/01/1998", "Đồng Nai", "Trường đời", "lớp J2225", "01234567890");
student.print();
student.print1();
console.log(student instanceof People);

let collegeStudent = new CollegeStudent("Trần Thanh Tú", "25/01/1998", "Đồng Nai", "Trường đời", "lớp J2225", "01234567890", "Developer", "tutt1");
collegeStudent.print();
collegeStudent.print1();
collegeStudent.print2();
console.log(collegeStudent instanceof People);

let worker = new Worker("Trần Thanh Tú", "25/01/1998", "Đồng Nai", "Thất nghiệp", "Ở nhà", "0");
worker.print();
worker.print3();
console.log(worker instanceof People);